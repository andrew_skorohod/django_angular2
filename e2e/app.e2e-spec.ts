import { NgfoldPage } from './app.po';

describe('ngfold App', () => {
  let page: NgfoldPage;

  beforeEach(() => {
    page = new NgfoldPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
